﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Web.Script.Serialization;
using System.Collections;
using System.Linq;

namespace MsTool
{
    public partial class MsToolForm : Form
    {
        public MsToolForm()
        {
            InitializeComponent();
        }
        public string LoadStr { get; set; } = "";
        private void bt_path_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            openFile.Filter = "文本文件|*.txt|所有文件|*.*";
            openFile.RestoreDirectory = true;
            openFile.FilterIndex = 1;
            string json = "";
            if (openFile.ShowDialog() == DialogResult.OK)
            {
                string  fName = openFile.FileName;
                txt_path.Text = openFile.FileName;
                StreamReader reader = new StreamReader(fName, Encoding.UTF8);
                LoadStr=reader.ReadToEnd().Trim();
                if (LoadStr!="")
                {
                    MatchCollection ms = Regex.Matches(LoadStr, "this.EquipDic\\[\\d+\\].*?(\\{.*?\\})", RegexOptions.Singleline | RegexOptions.IgnorePatternWhitespace);
                    List<string> alllist = new List<string>();
                    if (ms.Count>0)
                    {
                        foreach (Match item in ms)
                        {
                            alllist.Add(item.Groups[1].Value);
                        }
                    }
                    if (alllist!=null&&alllist.Count>0)
                    {
                        json = $"[{string.Join(",", alllist.ToArray())}]";
                    }
                }

            }
            try
            {
                DataTable dt = ToolJson.ToDataTable(json);
                var dataresult = dt.AsEnumerable();

                //排序
                var result = dataresult.OrderBy(x => x.Field<int>("ID"))
                    .OrderBy(x => x.Field<string>("Name"))
                    .OrderBy(x => x.Field<int>("Type"))
                    .OrderBy(x => x.Field<int>("Grade"))
                    .OrderBy(x => x.Field<int>("NeedLevel"));
 
                dg_data.DataSource = result.CopyToDataTable();
            }
            catch (Exception ex)
            {

                
            }
           


        }

        private void bt_import_Click(object sender, EventArgs e)
        {
            if (dg_data.Rows.Count > 0)
            {
                ExcelHelper.ExportToExcel(dg_data.ToDataTable());//Run
              
            }
            
        }
    }
}
