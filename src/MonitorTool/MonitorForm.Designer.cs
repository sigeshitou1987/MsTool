﻿namespace MonitorTool
{
    partial class MonitorForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MonitorForm));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txt_memory = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_filter = new System.Windows.Forms.TextBox();
            this.chk_CheckOpen = new System.Windows.Forms.CheckBox();
            this.txt_TimeSetting = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.bt_stop = new System.Windows.Forms.Button();
            this.bt_start = new System.Windows.Forms.Button();
            this.txt_sleeptime = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_number = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_times = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_path = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.rich_info = new System.Windows.Forms.RichTextBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItemshow = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemHide = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemexit = new System.Windows.Forms.ToolStripMenuItem();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.txt_cpu = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.rich_info);
            this.splitContainer1.Size = new System.Drawing.Size(622, 445);
            this.splitContainer1.SplitterDistance = 184;
            this.splitContainer1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.txt_cpu);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txt_memory);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txt_filter);
            this.groupBox1.Controls.Add(this.chk_CheckOpen);
            this.groupBox1.Controls.Add(this.txt_TimeSetting);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.bt_stop);
            this.groupBox1.Controls.Add(this.bt_start);
            this.groupBox1.Controls.Add(this.txt_sleeptime);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txt_number);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txt_times);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txt_path);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(622, 184);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "启动信息";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(277, 129);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(107, 12);
            this.label7.TabIndex = 16;
            this.label7.Text = "进程内存大小(KB):";
            // 
            // txt_memory
            // 
            this.txt_memory.Location = new System.Drawing.Point(390, 126);
            this.txt_memory.Name = "txt_memory";
            this.txt_memory.Size = new System.Drawing.Size(104, 21);
            this.txt_memory.TabIndex = 15;
            this.txt_memory.Text = "10000";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 129);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 12);
            this.label6.TabIndex = 14;
            this.label6.Text = "过滤配置:";
            // 
            // txt_filter
            // 
            this.txt_filter.Location = new System.Drawing.Point(82, 126);
            this.txt_filter.Name = "txt_filter";
            this.txt_filter.Size = new System.Drawing.Size(192, 21);
            this.txt_filter.TabIndex = 13;
            this.txt_filter.Text = "101-110|121-130";
            // 
            // chk_CheckOpen
            // 
            this.chk_CheckOpen.AutoSize = true;
            this.chk_CheckOpen.Location = new System.Drawing.Point(287, 100);
            this.chk_CheckOpen.Name = "chk_CheckOpen";
            this.chk_CheckOpen.Size = new System.Drawing.Size(120, 16);
            this.chk_CheckOpen.TabIndex = 12;
            this.chk_CheckOpen.Text = "启用不同时间轮询";
            this.chk_CheckOpen.UseVisualStyleBackColor = true;
            this.chk_CheckOpen.CheckedChanged += new System.EventHandler(this.Chk_CheckOpen_CheckedChanged);
            // 
            // txt_TimeSetting
            // 
            this.txt_TimeSetting.Location = new System.Drawing.Point(82, 95);
            this.txt_TimeSetting.Name = "txt_TimeSetting";
            this.txt_TimeSetting.Size = new System.Drawing.Size(192, 21);
            this.txt_TimeSetting.TabIndex = 11;
            this.txt_TimeSetting.Text = "9:00&500|10:00&600";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 98);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 12);
            this.label5.TabIndex = 10;
            this.label5.Text = "输入路径:";
            // 
            // bt_stop
            // 
            this.bt_stop.Location = new System.Drawing.Point(500, 95);
            this.bt_stop.Name = "bt_stop";
            this.bt_stop.Size = new System.Drawing.Size(91, 37);
            this.bt_stop.TabIndex = 9;
            this.bt_stop.Text = "停止监控 -WerFault.exe";
            this.bt_stop.UseVisualStyleBackColor = true;
            this.bt_stop.Click += new System.EventHandler(this.bt_stop_Click);
            // 
            // bt_start
            // 
            this.bt_start.Location = new System.Drawing.Point(500, 45);
            this.bt_start.Name = "bt_start";
            this.bt_start.Size = new System.Drawing.Size(91, 37);
            this.bt_start.TabIndex = 8;
            this.bt_start.Text = "开始监控 -WerFault.exe";
            this.bt_start.UseVisualStyleBackColor = true;
            this.bt_start.Click += new System.EventHandler(this.bt_start_Click);
            // 
            // txt_sleeptime
            // 
            this.txt_sleeptime.Location = new System.Drawing.Point(371, 61);
            this.txt_sleeptime.Name = "txt_sleeptime";
            this.txt_sleeptime.Size = new System.Drawing.Size(55, 21);
            this.txt_sleeptime.TabIndex = 7;
            this.txt_sleeptime.Text = "200";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(282, 66);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 12);
            this.label4.TabIndex = 6;
            this.label4.Text = "延时时间(ms):";
            // 
            // txt_number
            // 
            this.txt_number.Location = new System.Drawing.Point(219, 61);
            this.txt_number.Name = "txt_number";
            this.txt_number.Size = new System.Drawing.Size(55, 21);
            this.txt_number.TabIndex = 5;
            this.txt_number.Text = "500";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(154, 66);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "处理个数:";
            // 
            // txt_times
            // 
            this.txt_times.Location = new System.Drawing.Point(83, 61);
            this.txt_times.Name = "txt_times";
            this.txt_times.Size = new System.Drawing.Size(55, 21);
            this.txt_times.TabIndex = 3;
            this.txt_times.Text = "60";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "时钟/秒:";
            // 
            // txt_path
            // 
            this.txt_path.Location = new System.Drawing.Point(83, 23);
            this.txt_path.Name = "txt_path";
            this.txt_path.Size = new System.Drawing.Size(412, 21);
            this.txt_path.TabIndex = 1;
            this.txt_path.Text = "d:\\ms";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "输入路径:";
            // 
            // rich_info
            // 
            this.rich_info.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rich_info.Location = new System.Drawing.Point(0, 0);
            this.rich_info.Name = "rich_info";
            this.rich_info.Size = new System.Drawing.Size(622, 257);
            this.rich_info.TabIndex = 0;
            this.rich_info.Text = "";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemshow,
            this.toolStripMenuItemHide,
            this.toolStripMenuItemexit});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(101, 70);
            // 
            // toolStripMenuItemshow
            // 
            this.toolStripMenuItemshow.Name = "toolStripMenuItemshow";
            this.toolStripMenuItemshow.Size = new System.Drawing.Size(100, 22);
            this.toolStripMenuItemshow.Text = "显示";
            this.toolStripMenuItemshow.Click += new System.EventHandler(this.toolStripMenuItemshow_Click);
            // 
            // toolStripMenuItemHide
            // 
            this.toolStripMenuItemHide.Name = "toolStripMenuItemHide";
            this.toolStripMenuItemHide.Size = new System.Drawing.Size(100, 22);
            this.toolStripMenuItemHide.Text = "隐藏";
            this.toolStripMenuItemHide.Click += new System.EventHandler(this.toolStripMenuItemHide_Click);
            // 
            // toolStripMenuItemexit
            // 
            this.toolStripMenuItemexit.Name = "toolStripMenuItemexit";
            this.toolStripMenuItemexit.Size = new System.Drawing.Size(100, 22);
            this.toolStripMenuItemexit.Text = "退出";
            this.toolStripMenuItemexit.Click += new System.EventHandler(this.toolStripMenuItemexit_Click);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.ContextMenuStrip = this.contextMenuStrip1;
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "灭神监控程序";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.DoubleClick += new System.EventHandler(this.notifyIcon1_DoubleClick);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(277, 157);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(83, 12);
            this.label8.TabIndex = 18;
            this.label8.Text = "CPU占用比(%):";
            // 
            // txt_cpu
            // 
            this.txt_cpu.Location = new System.Drawing.Point(390, 154);
            this.txt_cpu.Name = "txt_cpu";
            this.txt_cpu.Size = new System.Drawing.Size(104, 21);
            this.txt_cpu.TabIndex = 17;
            this.txt_cpu.Text = "10";
            // 
            // MonitorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(622, 445);
            this.Controls.Add(this.splitContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MonitorForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "监控软件";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MonitorForm_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MonitorForm_FormClosed);
            this.Load += new System.EventHandler(this.MonitorForm_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_path;
        private System.Windows.Forms.TextBox txt_times;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_number;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_sleeptime;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button bt_start;
        private System.Windows.Forms.RichTextBox rich_info;
        private System.Windows.Forms.Button bt_stop;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemshow;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemHide;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemexit;
        private System.Windows.Forms.CheckBox chk_CheckOpen;
        private System.Windows.Forms.TextBox txt_TimeSetting;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txt_filter;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txt_memory;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txt_cpu;
    }
}

