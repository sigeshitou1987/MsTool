﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MonitorTool
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
          
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            System.Threading.Mutex mutex = new System.Threading.Mutex(false, "灭神监控程序");
            bool Running = !mutex.WaitOne(0, false);
            if (!Running)
                Application.Run(new MonitorForm());
            else
                MessageBox.Show("灭神监控程序已启动！");
           
        }
    }
}
